#!/bin/bash
# Specify the directory containing the folders you want to zip
base_directory="$( dirname -- "$0"; )"
cd $base_directory
# Loop through each folder in the directory
for folder_name in *; do
    # Check if it's a directory
    if [ -d "$folder_name" ]; then
        # Fichier metadata.txt
        # metadata_file="metadata.txt"
        # Récupérer la valeur actuelle de la version
        # current_version=$(grep '^version=' "$metadata_file" | cut -d= -f2)
        # Ajouter 0.1 à la version actuelle
        # new_version=$(echo "$current_version + 0.1" | bc)
        # sed -i "s/^version=$current_version$/version=$new_version/" "$metadata_file"
        # Create a zip file with the same name as the folder
        zip_filename="${folder_name##*/}.zip"

        # Zip the contents of the folder, including the top-level directory
        zip -qr "$zip_filename" "$folder_name"
    fi
done